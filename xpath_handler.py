import datetime
import lxml.html

from lxml import etree
from xml.etree import ElementTree
from xquery import *


if __name__ == "__main__":
    tree = etree.parse("new_file.xml")

    query = "count(/root/*)"
    print(f"Get the total number of items described\n{tree.xpath(query)}\n")

    print(f"Output information by one of the characteristics for all of the complex elements described")
    for item in tree.xpath("/root/item/*"):
        print(f"{item.tag}: {item.text}")
    print("\n")

    print(f"Select information for one characteristic2 for one folding element.")
    for item in tree.xpath("/root/item[1]/*"):
        print(f"{item.tag}: {item.text}")
    print("\n")

    print("Output the number of an element whose name consists of more than one word")
    for index in tree_xpath(tree, "index-of(/root/item/title[contains(., ' ')])"):
        print(index, end=' ')
    print("\n")

    print(f"Print only the first parameter of one complex element")
    for item in tree.xpath("/root/item[1]/*[1]"):
        print(f"{item.tag}: {item.text}")
    print('\n')

    print(f"Output only the second parameter of one complex element")
    for item in tree.xpath("/root/item[1]/*[2]"):
        print(f"{item.tag}: {item.text}")
    print('\n')

    print(f"Output only the third parameter of one complex element")
    for item in tree.xpath("/root/item[1]/*[3]"):
        print(f"{item.tag}: {item.text}")
    print('\n')

    print(f"Output information on two parameters of every fifth element")
    for item in tree.xpath("/root/item[(position() mod 5) = 1]"):
        print(f"{item[0].text}\n{item[2].text}")
    print("\n")

    print(f"Output the item number (goods, services, etc.) and, in addition, one more parameter (eg, price) for each other complex item in the document")
    for item in tree_xpath_(tree, "index-of(/root/item[(position() mod 2) = 1])"):
        print(f"{item[0]} - {item[1][0].text}")
