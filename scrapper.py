"""
Python web-scraper to retrieve xml data
"""

import datetime
import random
import re
import requests
import uuid
import json

from bs4 import BeautifulSoup
from dicttoxml import dicttoxml
from lxml import etree

SITE_URL = "https://medium.com/topic/data-science"
XML_FILE_NAME = "file.xml"
SCHEMA_FILE_NAME = "schema.xsd"
RECORDS_COUNT = 5
RECORDS_START = 0


def set_or_empty_str(list_, id_, field_, tree_):
    try:
        list_[id_][field_] = str(tree_.contents[0])
    except AttributeError:
        list_[id_][field_] = ""


def get_articles():
    page = requests.get(SITE_URL)

    soup = BeautifulSoup(page.content, "lxml")

    articles_html = soup.findAll("section", {"class": ["hy", "hz", "ai", "dp", "r", "ia", "ib", "ic", "id", "ie"]})

    articles = list()

    for pid, article_html in enumerate(articles_html[RECORDS_START:RECORDS_COUNT + RECORDS_START]):
        articles.append(dict())

        print(article_html)

        articles[pid]["title"] = str(article_html.find("h3", {"class": "ap q ec bw ed bx ge hq hr as av gf ei ej au"}).find("a").contents[0])
        articles[pid]["snippet"] = str(article_html.find("h3", {"class": "bw gc gd ge av gf ei as ej au cb"}).find("a").contents[0])

        author_resource = article_html.find("span", {"class": ["bw b bx by bz ca as av el ei ej au ap q"]}).findAll("a")
        if len(author_resource) == 2:
            author, resource = author_resource
        elif len(author_resource) == 1:
            author, resource = author_resource[0], ""
        else:
            author, resource = "", ""

        set_or_empty_str(articles, pid, "author", author)
        set_or_empty_str(articles, pid, "resource", resource)

        date_reading = article_html.find("div", {"class": "gq n co"})
        date = str(date_reading.contents[0])
        reading = str(date_reading).split("</span></div>")[-1][:-6]

        set_or_empty_str(articles, pid, "date", date)
        set_or_empty_str(articles, pid, "reading", reading)

        for aid, func in enumerate([uuid.uuid1, uuid.uuid4, datetime.datetime.now, random.random]):
            articles[pid][f"custom_field_{aid + 1}"] = str(func()) if aid < 2 else func()

    return articles


def validate(xml_path: str, xsd_path: str) -> bool:
    xmlschema_doc = etree.parse(xsd_path)
    xmlschema = etree.XMLSchema(xmlschema_doc)

    xml_doc = etree.parse(xml_path)
    result = xmlschema.validate(xml_doc)

    return result


if __name__ == "__main__":
    articles = get_articles()

    print(articles)

    with open(XML_FILE_NAME, "w", encoding="utf-8") as file:
        file.write(dicttoxml(articles).decode("utf-8"))

    print(validate("file.xml", "schema.xsd"))
