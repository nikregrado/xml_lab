import langid

from rdflib import (
    Graph,
    URIRef,
    BNode,
    Literal
)
from rdflib.namespace import (
    DC
)
from xml.etree import ElementTree


SITE_URL = "https://medium.com/topic/data-science"
XML_FILE_NAME = "file.xml"
FIELDS = {field: index for index, field in enumerate([
    'title', 'snippet', 'author', 'resource', 'date',
    'reading'
])}


def to_RDF(tree, graph):
    root = tree.getroot()
    root_ = URIRef(SITE_URL)

    for index, item in enumerate(root[::]):
        item_ = BNode(f"article_{index}")

        for field, field_index in FIELDS.items():
            item_field_ = Literal(item[field_index].text)
            graph.add((item_, Literal(DC.extent), item_field_))
        graph.add((
            root_,
            DC.extent,
            item_
        ))
    graph.add((root_, DC.has_part, Literal("articles")))
    return graph


if __name__ == "__main__":
    graph = Graph()
    tree = ElementTree.parse(XML_FILE_NAME)

    g = to_RDF(tree, graph)
    g.serialize("file.rdf", format="pretty-xml")

    print(f"1. Len of all triplets: {len(g)}\n")

    print(f"3. All triplets with type \"source-\" \
        (\"суб’єкт-предикат-об’єкт\"):")
    for s, p, o in g:
        print(s, p, o)
    print(f"\n")

    print("4. Вивести список усіх англомовних елементів із моделі даних.")
    for *_, o in g:
        lang = langid.classify(o)[0]
        if lang == "en":
            print(o)
