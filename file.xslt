<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <div class="toc-contents">
            <ul>
                <xsl:apply-templates/>
            </ul>
        </div>
    </xsl:template>
    <xsl:template match="item">
        <li>
            <div class="toc-item">
                <p>Title: <xsl:value-of select="title"/></p>
                <p>Snippet: <xsl:value-of select="snippet"/></p>
                <p>Author: <xsl:value-of select="author"/></p>
                <p>Date: <xsl:value-of select="date"/></p>
                <p>Reading: <xsl:value-of select="reading"/></p>
                <p>Custom Field 1: <xsl:value-of select="custom_field_1"/></p>
                <p>Custom Field 2: <xsl:value-of select="custom_field_2"/></p>
                <p>Custom Field 3: <xsl:value-of select="custom_field_3"/></p>
                <p>Custom Field 4: <xsl:value-of select="custom_field_4"/></p>
                <p>Custom Field New: <xsl:value-of select="custom_field_new"/></p>
            </div>
        </li>
    </xsl:template>
</xsl:stylesheet>