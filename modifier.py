import datetime
import lxml.html

from lxml import etree
from xml.etree import ElementTree


SITE_URL = "https://medium.com/topic/data-science"
XML_FILE_NAME = "file.xml"
NEW_XML_FILE_NAME = "new_file.xml"
XSLT_FILE_NAME = "file.xslt"
SCHEMA_FILE_NAME = "schema.xsd"
OUTPUT_HTML_FILE_NAME = "file.html"


FIELDS = {field: index for index, field in enumerate([
    'title', 'snippet', 'author', 'resource', 'date',
    'reading', 'custom_field_1', 'custom_field_2', 
    'custom_field_3', 'custom_field_4',
])}


def modify():
    tree = ElementTree.parse(XML_FILE_NAME)
    root = tree.getroot()

    for index, field in enumerate(root[::2]):
        root[index][FIELDS['author']].text = 'Mykola Dudnik'
        root[index].remove(root[index][FIELDS['custom_field_4']])

        item = ElementTree.SubElement(root[index], 'custom_field_new', {'type': 'datetime'})
        item.text = str(datetime.datetime.now())

    root.remove(root[0])

    item = ElementTree.SubElement(root, 'item', {'type': 'dict'})
    ElementTree.SubElement(item, 'title', {'type': 'str'}).text = 'new attribute'
    ElementTree.SubElement(item, 'snippet', {'type': 'str'}).text = 'new snippet'
    ElementTree.SubElement(item, 'author', {'type': 'str'}).text = 'new author'
    ElementTree.SubElement(item, 'resource', {'type': 'str'}).text = 'new resource'
    ElementTree.SubElement(item, 'date', {'type': 'str'}).text = 'new date'
    ElementTree.SubElement(item, 'reading', {'type': 'str'}).text = 'new reading'
    ElementTree.SubElement(item, 'custom_field_1', {'type': 'str'}).text = 'new custom_field_1'
    ElementTree.SubElement(item, 'custom_field_2', {'type': 'str'}).text = 'new custom_field_2'
    ElementTree.SubElement(item, 'custom_field_3', {'type': 'str'}).text = 'new custom_field_3'
    ElementTree.SubElement(item, 'custom_field_4', {'type': 'str'}).text = 'new custom_field_4'

    tree.write(NEW_XML_FILE_NAME)


def generate_html():
    xslt_doc = etree.parse(XSLT_FILE_NAME)
    xslt_transformer = etree.XSLT(xslt_doc)
    
    source_doc = etree.parse(NEW_XML_FILE_NAME)
    output_doc = xslt_transformer(source_doc)
    
    output_doc.write(OUTPUT_HTML_FILE_NAME, pretty_print=True)

if __name__ == "__main__":
    modify()
    generate_html()

